# Ejemplo ViewModel

Esta aplicación muestra un ejemplo de cómo usar **ViewModel** para almacenar los datos (en este caso un número entero) y no perderlos al cambiar la configuración de la Activity (ej. al girar la pantalla).

Consideraciones:



1. En el archivo _build.gradle_ se puede observar que se incluye una dependencia: <code>implementation <strong>"androidx.lifecycle:lifecycle-extensions:2.2.0"</strong></code>
2. Los datos que se quieran almacenar deben estar en una clase fuera de la Activity, esta clase debe ser heredera de <strong>ViewModel</strong>.
3. En la Activity deberá crearse una variable de clase de ese tipo (MainActivityViewModel en el ejemplo) que tiene una forma especial para instanciarse, a través de un proveedor especial, véase:


```
viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
