package es.acj.android.ejemplos;

import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {
    private int numero = 0;

    public void incrementar() {
        numero++;
    }

    public int getNumero() {
        return numero;
    }
}
