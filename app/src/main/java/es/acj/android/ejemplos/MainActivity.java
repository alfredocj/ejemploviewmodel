package es.acj.android.ejemplos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import es.acj.android.ejemplos.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;
    private int numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hago el BindingView
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        // le digo al botón que se ejecute aquí al ser pulsado
        binding.btIncrementar.setOnClickListener(this);
        // inicializo el ViewModel
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        // y actualizo los valores numéricos de la vista
        actualizarValoresVista();
    }

    @Override
    public void onClick(View v) {
        // incremento la variable y el valor del ViewModel
        numero++;
        viewModel.incrementar();
        // y actualizo la vista
        actualizarValoresVista();
    }

    public void actualizarValoresVista() {
        binding.tvNumVariable.setText(Integer.toString(numero));
        binding.tvNumModelView.setText(Integer.toString(viewModel.getNumero()));
    }
}